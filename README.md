# Robot framework container

This directory contains the Dockerfile for Robot framework. 

Robot Framework 3.1.2
GECKO DRIVER v0.24.0
Cromedriver 7.3
Python 3.6
Seleniumlibrary 3.3.1

## How to run

To build the Robot Docker image, run

```
docker build -t rf-docker .
```

To run the sample tests on the container, run

```
docker run --rm -e ROBOT_TESTS=/sample_tests/ -e OUTPUT_DIR=/output/ -v $(pwd)/sample_tests:/sample_tests -v $(pwd)/output:/output -ti rf-docker
```

## GitLab CI/CD pipeline usage

To use this Docker image in your CI/CD pipeline simply add the following (and modify when needed) to your `.gitlab-ci.yml` file.

```
stages:
  - test

test:
  stage: test
  image: gitlab.labranet.jamk.fi:4567/wimma-lab-2019/mysticons/cayac/tools/robot
  script:
    - export ROBOT_TESTS=PATH_TO_TESTS_DIRECTORY/
    - export OUTPUT_DIR=DIRECTORY_TO_SAVE_OUTPUT_TO
    - run.sh
  artifacts:
    paths:
      - DIRECTORY_TO_SAVE_OUTPUT_TO/
```

A noteworthy thing about writing RF tests for Chrome is that you need to create a custom webdriver for Chrome. The needed steps can be created as a keyword. An example for test written for Chrome is below.

```
*** Keywords ***
Start Chrome
    ${chrome_options}=    Evaluate    sys.modules['selenium.webdriver'].ChromeOptions()    sys, selenium.webdriver
    Call Method    ${chrome_options}    add_argument    --no-sandbox
    Create Webdriver    Chrome      chrome_options=${chrome_options}

*** Test Cases ***
Test Chrome
    Start Chrome
    Go To   http://www.google.com
    Title Should Be     Google
    Close Browser
```